FROM python:3.9.19

WORKDIR /mnt/mloader

ENV DEBIAN_FRONTEND=noninteractive

RUN pip install --no-cache-dir mloader

RUN apt update && apt install -y rename && rm -rf /var/lib/apt/lists/*

# Change CMD to let mloader to what you want.
CMD ["mloader", "--help"]
